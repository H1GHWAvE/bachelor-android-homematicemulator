[![Code Climate](https://codeclimate.com/github/H1GHWAvE/de.tum.neutze.homematicemulator.png)](https://codeclimate.com/github/H1GHWAvE/de.tum.neutze.homematicemulator)


de.tum.neutze.homematicemulator
===============================

an Android application emulating a HomeMatic CCU

Bachelor's Thesis WS 2012/2013 at the Technische Universität München
Creator: Johannes Neutze, johannes.neutze@gmail.com
