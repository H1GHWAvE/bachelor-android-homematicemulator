package de.tum.neutze.homematic.database;

/**
* @author Johannes Neutze
* @version 1.0
* @since 1.6
*/

public class Position {
	long _id;
	String channel_id;
	long x_coord;
	long y_coord;
	long buffer;
	
	/**
	 * getters for the class
	 * sets values for the object
	 * @return requested parameter
	 */
	public long get_id() {
		return _id;
	}
	public String getChannel_id() {
		return channel_id;
	}
	public long getX_coord() {
		return x_coord;
	}
	public long getY_coord() {
		return y_coord;
	}
	public long getBuffer() {
		return buffer;
	}
	
	/**
	 * setters for the class
	 * get values from the object
	 * @param parameter to set
	 */
	public void set_id(long _id) {
		this._id = _id;
	}
	public void setChannel_id(String channel_id) {
		this.channel_id = channel_id;
	}
	public void setX_coord(long x_coord) {
		this.x_coord = x_coord;
	}
	public void setY_coord(long y_coord) {
		this.y_coord = y_coord;
	}
	public void setBuffer(long buffer) {
		this.buffer = buffer;
	}
	
}
