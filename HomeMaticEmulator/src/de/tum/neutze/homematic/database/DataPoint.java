package de.tum.neutze.homematic.database;

/**
* @author Johannes Neutze
* @version 1.0
* @since 1.6
*/

public class DataPoint {
	long _id;
	String channel_type;
	String name;
	String type;
	String access_type;
	String min;
	String max;
	String unit;
	String stdValue;
	
	/**
	 * getters for the class
	 * sets values for the object
	 * @return requested parameter
	 */

	public long get_id() {
		return _id;
	}
	public String getChannel_type() {
		return channel_type;
	}
	public String getName() {
		return name;
	}
	public String getType() {
		return type;
	}
	public String getAccess_type() {
		return access_type;
	}
	public String getMin() {
		return min;
	}
	public String getMax() {
		return max;
	}
	public String getUnit() {
		return unit;
	}
	public String getStdValue() {
		return stdValue;
	}
	
	/**
	 * setters for the class
	 * get values from the object
	 * @param parameter to set
	 */
	public void set_id(long _id) {
		this._id = _id;
	}
	public void setChannel_type(String channel_type) {
		this.channel_type = channel_type;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setType(String type) {
		this.type = type;
	}
	public void setAccess_type(String access_type) {
		this.access_type = access_type;
	}
	public void setMin(String min) {
		this.min = min;
	}
	public void setMax(String max) {
		this.max = max;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public void setStdValue(String stdValue) {
		this.stdValue = stdValue;
	}
}
