package de.tum.neutze.homematic.database;

/**
* @author Johannes Neutze
* @version 1.0
* @since 1.6
*/

public class Function {
	long _id;
	String name;
	String description;
	
	/**
	 * getters for the class
	 * sets values for the object
	 * @return requested parameter
	 */
	public long get_id() {
		return _id;
	}
	public String getName() {
		return name;
	}
	public String getDescription() {
		return description;
	}
	
	/**
	 * setters for the class
	 * get values from the object
	 * @param parameter to set
	 */
	public void set_id(long _id) {
		this._id = _id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * returns the string for the class
	 * @return concatenated string of name and description
	 */
	public String toString() {
		//if no description is empty
		if (description.length() == 0) {
			description = "no description";
		}
		return name + " : " + '"' + description + '"';
	}
}
