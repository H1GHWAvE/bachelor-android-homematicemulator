package de.tum.neutze.homematic.database;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.tum.neutze.homematic.server.NanoHTTPD;
import de.tum.neutze.homematic.activities.MainActivity;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.widget.Toast;

/**
* @author Johannes Neutze
* @version 1.0
* @since 1.6
* code snippets are based on/adapted from:
* [1]
* Lars Vogel
* http://www.vogella.com/articles/AndroidSQLite/article.html
* Eclipse Public License 
*/

public class DataSource {
	//definitions of SQLiteDatabase and DataHelper
	public static SQLiteDatabase homeMaticDatabase;
	private DatabaseHelper dbHelper;
	
	/**
	 * table rows summarized in a String[] for easy use
	 */
	public static String[] allDatabaseHelper = {DatabaseHelper.COLUMN_META_LOCALE};
	public static String[] allColumnsHWDevice = {
	  DatabaseHelper.COLUMN_HWDEVICE_ID, 
	  DatabaseHelper.COLUMN_HWDEVICE_TYPE, 
	  DatabaseHelper.COLUMN_HWDEVICE_HW };	
	public static String[] allColumnsDevicetype = {
	  DatabaseHelper.COLUMN_DEVICETYPE_ID, 
	  DatabaseHelper.COLUMN_DEVICETYPE_FORGET_IT, 
	  DatabaseHelper.COLUMN_DEVICETYPE_HW, 
	  DatabaseHelper.COLUMN_DEVICETYPE_CNT, 
	  DatabaseHelper.COLUMN_DEVICETYPE_CHANNEL_TYPE };		
	public static String[] allColumnsDP = {		
		DatabaseHelper.COLUMN_DP_ID,
		DatabaseHelper.COLUMN_DP_CHANNEL_TYPE,
		DatabaseHelper.COLUMN_DP_NAME,
		DatabaseHelper.COLUMN_DP_TYPE,
		DatabaseHelper.COLUMN_DP_ACCESS_TYPE,
		DatabaseHelper.COLUMN_DP_MIN,
		DatabaseHelper.COLUMN_DP_MAX,
		DatabaseHelper.COLUMN_DP_UNIT,
		DatabaseHelper.COLUMN_DP_STDVALUE };
	public static String[] allColumnsChannel = {	
		DatabaseHelper.COLUMN_CHANNEL_ID,
		DatabaseHelper.COLUMN_CHANNEL_NAME,
		DatabaseHelper.COLUMN_CHANNEL_ADDRESS,
		DatabaseHelper.COLUMN_CHANNEL_PARENT_DEVICE,
		DatabaseHelper.COLUMN_CHANNEL_DEVICE_INDEX,
		DatabaseHelper.COLUMN_CHANNEL_FUNCTION_ID,
		DatabaseHelper.COLUMN_CHANNEL_ROOM_ID };
	public static String[] allColumnsDevice = {	
		DatabaseHelper.COLUMN_DEVICE_ID,
		DatabaseHelper.COLUMN_DEVICE_NAME,
		DatabaseHelper.COLUMN_DEVICE_ADDRESS,
		DatabaseHelper.COLUMN_DEVICE_DEVICE_INTERFACE,
		DatabaseHelper.COLUMN_DEVICE_TYPE };		
	public static String[] allColumnsFunction = {
		DatabaseHelper.COLUMN_FUNCTION_ID,
		DatabaseHelper.COLUMN_FUNCTION_NAME,
		DatabaseHelper.COLUMN_FUNCTION_DESCRIPTION }; 
	public static String[] allColumnsRoom = {	
		DatabaseHelper.COLUMN_ROOM_ID,
		DatabaseHelper.COLUMN_ROOM_NAME }; 
	public static String[] allColumnsPosition = {		
		DatabaseHelper.COLUMN_POSITION_ID,
		DatabaseHelper.COLUMN_POSITION_CHANNEL_ID,
		DatabaseHelper.COLUMN_POSITION_X_COORD,
		DatabaseHelper.COLUMN_POSITION_Y_COORD,
		DatabaseHelper.COLUMN_POSITION_SIZE }; 
	public static String[] allColumnsValue = {	
	  DatabaseHelper.COLUMN_VALUE_ID,
		DatabaseHelper.COLUMN_VALUE_NAME,
		DatabaseHelper.COLUMN_VALUE_VALUE_TYPE,
		DatabaseHelper.COLUMN_VALUE_STATE,
		DatabaseHelper.COLUMN_VALUE_TIMESTAMP,
		DatabaseHelper.COLUMN_VALUE_CHANNEL_ID,
		DatabaseHelper.COLUMN_VALUE_DP_ID };
  
	/**
	 * check if database must be created or is already existing
	 * @param context context from main activity
	 */
	  public DataSource(Context context) {
	    dbHelper = new DatabaseHelper(context);
		////NanoHTTPD.myOut.println("I'm alive");
	    try {
	    	dbHelper.createDataBase();
		} catch (IOException ioe) {
			throw new Error("Unable to create database");
		}
		try {
			dbHelper.openDataBase();
		}catch(SQLException sqle){
			throw sqle;
		}
	  }
	
	  /**
	   * make database writable
	   * @throws SQLException
	   */
	  public void open() throws SQLException {
	    homeMaticDatabase = dbHelper.getWritableDatabase();
	  }
	
	  
	  /**
	   * close database
	   */
	  public void close() {
	    dbHelper.close();
	  }
	
	  /**
	   * create channels helper method 
	   * @param device used to create key in channel
	   */
	  public static void createChannels(Device device) {
		List<DataPoint> datapoints = new ArrayList<DataPoint>();
		
		//get devicetype object matching the device
	    Cursor cursor = homeMaticDatabase.query(DatabaseHelper.TABLE_DEVICETYPE,
	            allColumnsDevicetype, DatabaseHelper.COLUMN_DEVICETYPE_HW + " = " + '"' + device.getType() + '"', null,
	            null, null, null);
	    cursor.moveToFirst();
	    DeviceType deviceType = cursorToDeviceType(cursor);
	    cursor.close();
	    
	    //get matching datapoints for the devicetype
	    Cursor cursorDP = homeMaticDatabase.query(DatabaseHelper.TABLE_DP,
	            allColumnsDP, DatabaseHelper.COLUMN_DP_CHANNEL_TYPE + " = " + '"' + deviceType.getChannel_type() + '"', null,
	            null, null, null);
	    cursorDP.moveToFirst();
	    //get all datapoints
	    while (!cursorDP.isAfterLast()) {
	      DataPoint datapoint = cursorToDataPoint(cursorDP);
	      datapoints.add(datapoint);
	      cursorDP.moveToNext();
	    }
	    cursor.close();
	    //create as many channels as deviceType.getCnt() with each having all datapoints
	    for (int j = 0; j < deviceType.getCnt(); j++) {
		    	createChannel(datapoints, device, j);	
	    }
	  }
	  
	  /**
	   * create channel for the device
	   * @param datapoints used for the value entries of the channel
	   * @param device used for referencing channel to the parent device
	   * @param number number of channel created for the device and used for the index
	   */
	  public static void createChannel(List<DataPoint> datapoints, Device device, int number) {
		    ContentValues values = new ContentValues();
		    
		    //values for channel
		    values.put(DatabaseHelper.COLUMN_CHANNEL_NAME, device.getName() + " - " + device.getAddress() + ":" + (number+1));
		    values.put(DatabaseHelper.COLUMN_CHANNEL_ADDRESS, device.getAddress() + ":" + (number+1));
		    values.put(DatabaseHelper.COLUMN_CHANNEL_PARENT_DEVICE, device.get_id());
		    values.put(DatabaseHelper.COLUMN_CHANNEL_DEVICE_INDEX, number+1);
		    values.put(DatabaseHelper.COLUMN_CHANNEL_FUNCTION_ID, "");
		    values.put(DatabaseHelper.COLUMN_CHANNEL_ROOM_ID, "");
		    
		    //insert channel into database
		    long insertId = homeMaticDatabase.insert(DatabaseHelper.TABLE_CHANNEL, null,
		        values);
		    //set cursor to get channel
		    Cursor cursor = homeMaticDatabase.query(DatabaseHelper.TABLE_CHANNEL,
		            allColumnsChannel, DatabaseHelper.COLUMN_CHANNEL_ID + " = " + insertId, null,
		            null, null, null);
		    cursor.moveToFirst();
		    Channel newChannel = cursorToChannel(cursor);
		    cursor.close();
		    
		    //create value entries for the channel
		    createValue(datapoints, newChannel);
		  }
	  
	  /**
	   * creates the value entries for the channel
	   * @param datapoints used as blueprint
	   * @param channel used as link in channel_id
	   */
	  public static void createValue(List<DataPoint> datapoints, Channel channel) {
		  //for each datapoint a value entry is created
		  for (int i = 0; i < datapoints.size(); i++) {
		    ContentValues values = new ContentValues();
		    
		    //values for the value entry
		    values.put(DatabaseHelper.COLUMN_VALUE_NAME, datapoints.get(i).getName() );
		    values.put(DatabaseHelper.COLUMN_VALUE_VALUE_TYPE, "4" );
		    values.put(DatabaseHelper.COLUMN_VALUE_STATE, datapoints.get(i).getStdValue());
		    values.put(DatabaseHelper.COLUMN_VALUE_TIMESTAMP, String.valueOf(System.currentTimeMillis()));
		    values.put(DatabaseHelper.COLUMN_VALUE_CHANNEL_ID, channel.get_id());
		    values.put(DatabaseHelper.COLUMN_VALUE_DP_ID, datapoints.get(i).get_id());
		    //insert into database
		    homeMaticDatabase.insert(DatabaseHelper.TABLE_VALUE, null, values);
		  }
	  }
	  
	  /**
	   * method to update values in the database
	   * @param id of the values' channel
	   * @param modifiedValues list of the new values for each value entry
	   */
	  public static void modifyValues(long id, List<String> modifiedValues) {
		  List<Value> values = new ArrayList<Value>();
		  //get all value entries
		  Cursor cursor = homeMaticDatabase.query(DatabaseHelper.TABLE_VALUE,
		            allColumnsValue, DatabaseHelper.COLUMN_VALUE_CHANNEL_ID + " = " + '"' + id + '"', null,
		            null, null, null);
		    cursor.moveToFirst();
		    while (!cursor.isAfterLast()) {
		      Value value = cursorToValue(cursor);
		      values.add(value);
		      cursor.moveToNext();
		    }
		  //add new value for each entry
		  for (int i = 0; i < values.size(); i++) {
		    ContentValues contentValues = new ContentValues();
		    contentValues.put(DatabaseHelper.COLUMN_VALUE_STATE, modifiedValues.get(i));
		    contentValues.put(DatabaseHelper.COLUMN_VALUE_TIMESTAMP, String.valueOf(System.currentTimeMillis()));
		    //update entry
		    homeMaticDatabase.update(DatabaseHelper.TABLE_VALUE, contentValues, "_id=" + values.get(i).get_id(), null);
		  }
	  }
	
	  /**
	   * delete a channel from the database
	   * @param parent device which was deleted and who's channels should be deleted as well
	   * @return if deleted
	   */
	  public static Boolean deleteChannel (long parent) {
		  //get all channels
		Cursor cursor = homeMaticDatabase.query(DatabaseHelper.TABLE_CHANNEL,
		allColumnsChannel, DatabaseHelper.COLUMN_CHANNEL_PARENT_DEVICE+ " = " + '"' + parent + '"', null,
		null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Channel channel = cursorToChannel(cursor);
			List<Position> positions = getAllPositions();
				for( int i = 0; i < positions.size(); i++) {
					if(positions.get(i).getChannel_id().equals(String.valueOf(channel.get_id())))
			 {
						//delete it's position entry
						deletePosition(positions.get(i));
			 		}
				}
		
		
			//delete channel from database
			homeMaticDatabase.delete(DatabaseHelper.TABLE_CHANNEL, DatabaseHelper.COLUMN_CHANNEL_ID
					+ " = " + channel.get_id(), null);
			//delete its values
			deleteValue(channel.get_id());
			cursor.moveToNext();
		}
			return true;
		}
	
	  /**
	   * delete a channel from the database
	   * @param parent channel which was deleted and who's values should be deleted as well
	   * @return if deleted
	   */
	  public static Boolean deleteValue (long parent) {
		    Cursor cursor = homeMaticDatabase.query(DatabaseHelper.TABLE_VALUE,
		            allColumnsValue, DatabaseHelper.COLUMN_VALUE_CHANNEL_ID+ " = " + '"' + parent + '"', null,
		            null, null, null);
		    cursor.moveToFirst();
		    while (!cursor.isAfterLast()) {
		    	////NanoHTTPD.myOut.println("execute value!");
		    	Value value = cursorToValue(cursor);
			    homeMaticDatabase.delete(DatabaseHelper.TABLE_VALUE, DatabaseHelper.COLUMN_VALUE_ID
		    		    + " = " + value.get_id(), null);;
		      cursor.moveToNext();
		    }
			return true;
		  }
	
	  /**
	   * returns all entries of the table as List<type>
	   * @return List<type> of all database entries
	   */
	  public static List<Channel> getAllChannels() {
	    List<Channel> channels = new ArrayList<Channel>();
	    //get all channels
	    Cursor cursor = homeMaticDatabase.query(DatabaseHelper.TABLE_CHANNEL,
	        allColumnsChannel, null, null, null, null, null);
	 
	    cursor.moveToFirst();
	    while (!cursor.isAfterLast()) {
	    	//get channel
	      Channel channel = cursorToChannel(cursor);
	      //add channel to list
	      channels.add(channel);
	      cursor.moveToNext();
	    }
	    cursor.close();
	    return channels;
	  }
	
	  /**
	   * turns database cursor into object
	   * @param cursor cursor to database entry which should be returned into an object
	   * @return object containing the database data
	   */
	  public static Channel cursorToChannel(Cursor cursor) {
		Channel channel = new Channel();
		channel.set_id(cursor.getLong(0));
		channel.setName(cursor.getString(1));
		channel.setAddress(cursor.getString(2));
		channel.setParent_device(cursor.getLong(3));
		channel.setDevice_index(cursor.getLong(4));
		channel.setFunction_id(cursor.getLong(5));
		channel.setRoom_id(cursor.getLong(6));
	    return channel;
	  }
	  
	  /**
	   * turns database cursor into object
	   * @param cursor cursor to database entry which should be returned into an object
	   * @return object containing the database data
	   */
	  public static DeviceType cursorToDeviceType(Cursor cursor) {
		DeviceType deviceType = new DeviceType();
		deviceType.set_id(cursor.getLong(0));
		deviceType.setForgetIt(cursor.getString(1));
		deviceType.setHw(cursor.getString(2));
		deviceType.setCnt(cursor.getLong(3));
		deviceType.setChannel_type(cursor.getString(4));
	    return deviceType;
	  }
	  
	  /**
	   * turns database cursor into object
	   * @param cursor cursor to database entry which should be returned into an object
	   * @return object containing the database data
	   */
	  public static Value cursorToValue(Cursor cursor) {
		Value value = new Value();
		value.set_id(cursor.getLong(0));
		value.setName(cursor.getString(1));
		value.setValue_type(cursor.getString(2));
		value.setState(cursor.getString(3));
		value.setTimestamp(cursor.getLong(4));
		value.setChannel_id(cursor.getLong(5));
		value.setDp_id(cursor.getLong(6));
	    return value;
	  }
	  
	  /**
	   * turns database cursor into object
	   * @param cursor cursor to database entry which should be returned into an object
	   * @return object containing the database data
	   */
	  public static DataPoint cursorToDataPoint(Cursor cursor) {
		DataPoint datapoint = new DataPoint();
		datapoint.set_id(cursor.getLong(0));
		datapoint.setChannel_type(cursor.getString(1));
		datapoint.setName(cursor.getString(2));
		datapoint.setType(cursor.getString(3));
		datapoint.setAccess_type(cursor.getString(4));
		datapoint.setMin(cursor.getString(5));
		datapoint.setMax(cursor.getString(6));
		datapoint.setUnit(cursor.getString(7));
		datapoint.setStdValue(cursor.getString(8));
	    return datapoint;
	  }
	  
	  /**
	   * returns all entries of the table as List<type>
	   * @return List<type> of all database entries
	   */
	  public static List<DataPoint> getAllDP() {
		    List<DataPoint> datapoints = new ArrayList<DataPoint>();
		    
		    Cursor cursor = homeMaticDatabase.query(DatabaseHelper.TABLE_DP,
		        allColumnsDP, null, null, null, null, null);
		 
		    cursor.moveToFirst();
		    while (!cursor.isAfterLast()) {
		      DataPoint datapoint = cursorToDataPoint(cursor);
		      datapoints.add(datapoint);
		      cursor.moveToNext();
		    }
		    // Make sure to close the cursor
		    cursor.close();
		    return datapoints;
		  }
	  
	  /**
	   * returns all entries of the table as List<type>
	   * @return List<type> of all database entries
	   */
	  public static List<HWDevice> getAllHWDevices() {
		    List<HWDevice> hwdevices = new ArrayList<HWDevice>();
		    
		    Cursor cursor = homeMaticDatabase.query(DatabaseHelper.TABLE_HWDEVICE,
		        allColumnsHWDevice, null, null, null, null, null);
		 
		    cursor.moveToFirst();
		    while (!cursor.isAfterLast()) {
		    	HWDevice hwdevice = cursorToHWDevice(cursor);
		    	hwdevices.add(hwdevice);
		    	cursor.moveToNext();
		    }
		    // Make sure to close the cursor
		    cursor.close();
		    return hwdevices;
		  }
	  
	  /**
	   * turns database cursor into object
	   * @param cursor cursor to database entry which should be returned into an object
	   * @return object containing the database data
	   */
	  public static HWDevice cursorToHWDevice(Cursor cursor) {
		  	HWDevice hwdevice = new HWDevice();
		    hwdevice.set_id(cursor.getLong(0));
		    hwdevice.setType(cursor.getString(1));
		    hwdevice.setHw(cursor.getString(2));
		    return hwdevice;
		  }
	
	  /**
	   * creates a new device entry
	   * @param name name for the device
	   * @param hw hardware address of the device
	   * @return
	   */
	  public static Device createDevice(String name, String hw) {
	    ContentValues values = new ContentValues();
	    values.put(DatabaseHelper.COLUMN_DEVICE_NAME, name);
	    values.put(DatabaseHelper.COLUMN_DEVICE_ADDRESS, hw);
	    values.put(DatabaseHelper.COLUMN_DEVICE_DEVICE_INTERFACE, "BidCos-RF");
	    values.put(DatabaseHelper.COLUMN_DEVICE_TYPE, hw);
	    long insertId = homeMaticDatabase.insert(DatabaseHelper.TABLE_DEVICE, null,
	        values);
	    String address = "JEQ";
	    int additionalZeros = 7 - String.valueOf(insertId).length();
	    for (int i = 0; i < additionalZeros; i++) {
	    	address+="0";
	    }
	    address+=String.valueOf(insertId);
	    values.put(DatabaseHelper.COLUMN_DEVICE_ADDRESS, address);
	    homeMaticDatabase.update(DatabaseHelper.TABLE_DEVICE, values, "_id=" + insertId, null);
	    Cursor cursor = homeMaticDatabase.query(DatabaseHelper.TABLE_DEVICE,
	            allColumnsDevice, DatabaseHelper.COLUMN_DEVICE_ID + " = " + insertId, null,
	            null, null, null);
	    cursor.moveToFirst();
	    Device newDevice = cursorToDevice(cursor);
	    cursor.close();
	    //NanoHTTPD.myOut.println("createChannels next");
	    createChannels(newDevice);
	    return newDevice;
	  }
	  
	  /**
	   * delete a device from the database
	   * @param device device to be deleted
	   * @return if deleted
	   */
	  public static boolean deleteDevice (Device device) {
		    long id = device.get_id();
		    //System.out.println("Device deleted with id: " + id);
		    homeMaticDatabase.delete(DatabaseHelper.TABLE_DEVICE, DatabaseHelper.COLUMN_DEVICE_ID
		        + " = " + id, null);
		    //NanoHTTPD.myOut.println("deleted " + id);
		    deleteChannel(id);
		    return true;
		  }
	
	  /**
	   * returns all entries of the table as List<type>
	   * @return List<type> of all database entries
	   */
	  public static List<Device> getAllDevices() {
	    List<Device> devices = new ArrayList<Device>();
	    
	    Cursor cursor = homeMaticDatabase.query(DatabaseHelper.TABLE_DEVICE,
	        allColumnsDevice, null, null, null, null, null);
	 
	    cursor.moveToFirst();
	    while (!cursor.isAfterLast()) {
	      Device device = cursorToDevice(cursor);
	      devices.add(device);
	      cursor.moveToNext();
	    }
	    // Make sure to close the cursor
	    cursor.close();
	    return devices;
	  }
	
	  /**
	   * turns database cursor into object
	   * @param cursor cursor to database entry which should be returned into an object
	   * @return object containing the database data
	   */
	  public static Device cursorToDevice(Cursor cursor) {
	    Device device = new Device();
	    device.set_id(cursor.getLong(0));
	    device.setName(cursor.getString(1));
	    device.setAddress(cursor.getString(2));
	    device.setDevice_interface(cursor.getString(3));
	    device.setType(cursor.getString(4));
	    return device;
	  }
	  
	  /**
	   * create a new function entry in the database
	   * @param name
	   * @param description
	   * @return
	   */
	  public static Function createFunction(String name, String description) {
		    ContentValues values = new ContentValues();
		    values.put(DatabaseHelper.COLUMN_FUNCTION_NAME, name);
		    values.put(DatabaseHelper.COLUMN_FUNCTION_DESCRIPTION, description);
		    long insertId = homeMaticDatabase.insert(DatabaseHelper.TABLE_FUNCTION, null,
		        values);
		    
		    Cursor cursor = homeMaticDatabase.query(DatabaseHelper.TABLE_FUNCTION,
		            allColumnsFunction, DatabaseHelper.COLUMN_FUNCTION_ID + " = " + insertId, null,
		            null, null, null);
		    cursor.moveToFirst();
		    Function newFunction = cursorToFunction(cursor);
		    cursor.close();
		    //NanoHTTPD.myOut.println("create function");
		    return newFunction;
		  }
	  
	  /**
	   * delete a function from the database
	   * @param function function to be deleted
	   * @return if deleted
	   */
	  public static boolean deleteFunction (Function function) {
		    long id = function.get_id();
		    //System.out.println("Function deleted with id: " + id);
		    homeMaticDatabase.delete(DatabaseHelper.TABLE_FUNCTION, DatabaseHelper.COLUMN_FUNCTION_ID
		        + " = " + id, null);
		    return true;
		  }

	  /**
	   * returns all entries of the table as List<type>
	   * @return List<type> of all database entries
	   */
	  public static List<Function> getAllFunctions() {
	    List<Function> functions = new ArrayList<Function>();
	    
	    Cursor cursor = homeMaticDatabase.query(DatabaseHelper.TABLE_FUNCTION,
	        allColumnsFunction, null, null, null, null, null);
	 
	    cursor.moveToFirst();
	    while (!cursor.isAfterLast()) {
	      Function function = cursorToFunction(cursor);
	      functions.add(function);
	      cursor.moveToNext();
	    }
	    // Make sure to close the cursor
	    cursor.close();
	    return functions;
	  }

	  /**
	   * turns database cursor into object
	   * @param cursor cursor to database entry which should be returned into an object
	   * @return object containing the database data
	   */
	  private static Function cursorToFunction(Cursor cursor) {
	    Function function = new Function();
	    function.set_id(cursor.getLong(0));
	    function.setName(cursor.getString(1));
	    function.setDescription(cursor.getString(2));
	    return function;
	  }
	  
	  /**
	   * creates a new room entry
	   * @param name name of the room
	   * @return created room
	   */
	  public static Room createRoom(String name) {
		    ContentValues values = new ContentValues();
		    values.put(DatabaseHelper.COLUMN_ROOM_NAME, name);
		    long insertId = homeMaticDatabase.insert(DatabaseHelper.TABLE_ROOM, null, values);
		    Cursor cursor = homeMaticDatabase.query(DatabaseHelper.TABLE_ROOM,
		    		allColumnsRoom, DatabaseHelper.COLUMN_ROOM_ID + " = " + insertId, null,
		            null, null, null);
		    cursor.moveToFirst();
		    Room newRoom = cursorToRoom(cursor);
		    cursor.close();
		    //NanoHTTPD.myOut.println("created room");
		    return newRoom;
		  }
	  /**
	   * delete a room from the database
	   * @param room room to be deleted
	   * @return if deleted
	   */
	  public static boolean deleteRoom (Room room) {
	    long id = room.get_id();
	    //System.out.println("Room deleted with id: " + id);
	    homeMaticDatabase.delete(DatabaseHelper.TABLE_ROOM, DatabaseHelper.COLUMN_ROOM_ID
	        + " = " + id, null);
	    return true;
	  }

	  /**
	   * returns all entries of the table as List<type>
	   * @return List<type> of all database entries
	   */
	  public static List<Room> getAllRooms() {
	    List<Room> rooms = new ArrayList<Room>();
	    Cursor cursor = homeMaticDatabase.query(DatabaseHelper.TABLE_ROOM, 
    		allColumnsRoom, null, null, null, null, null);
	    cursor.moveToFirst();
	    while (!cursor.isAfterLast()) {
	      Room room = cursorToRoom(cursor);
	      rooms.add(room);
	      cursor.moveToNext();
	    }
	    cursor.close();
	    return rooms;
		  }

	  /**
	   * delete a room from the database
	   * @param room room to be deleted
	   * @return if deleted
	   */
	  private static Room cursorToRoom(Cursor cursor) {
	    Room room = new Room();
	    room.set_id(cursor.getLong(0));
	    room.setName(cursor.getString(1));
	    return room;
	  }
	  
	  /**
	   * create a new position for a channel and edit that channel
	   * @param name new name of the channel
	   * @param channel the channel as object
	   * @param x_coord x coordinate of the new position
	   * @param y_coord y coordinate
	   * @param buffer
	   * @param room
	   * @param function
	   */
	  public static void createPosition(String name, Channel channel, String x_coord, String y_coord, int buffer, String room, String function) {
	    ContentValues values = new ContentValues();
	    values.put(DatabaseHelper.COLUMN_POSITION_CHANNEL_ID, channel.get_id());
	    values.put(DatabaseHelper.COLUMN_POSITION_X_COORD, x_coord);
	    values.put(DatabaseHelper.COLUMN_POSITION_Y_COORD, y_coord);
	    values.put(DatabaseHelper.COLUMN_POSITION_SIZE, buffer);
	    homeMaticDatabase.insert(DatabaseHelper.TABLE_POSITION, null, values);
	    //NanoHTTPD.myOut.println("position: " + insertId);

	    ContentValues contentValues = new ContentValues();
	    contentValues.put(DatabaseHelper.COLUMN_CHANNEL_NAME, name);
	    contentValues.put(DatabaseHelper.COLUMN_CHANNEL_FUNCTION_ID, function);
	    contentValues.put(DatabaseHelper.COLUMN_CHANNEL_ROOM_ID, room);
		homeMaticDatabase.update(DatabaseHelper.TABLE_CHANNEL, contentValues, "_id=" + channel.get_id(), null);
	  }
	  
	  /**
	   * 
	   * modify a channel's name, room_id and function_id
	   * @param id id of the room to be modified
	   * @param room room object is used to get its id and to link it in the room_id
	   * @param function function object is used to get its id and to link it in the function_id
	   */
	  public static void modifyChannel(long id, String room, String function) {
		    ContentValues contentValues = new ContentValues();
		    	contentValues.put(DatabaseHelper.COLUMN_CHANNEL_FUNCTION_ID, function);
			    NanoHTTPD.myOut.println("modified room");

		    			   
		    	contentValues.put(DatabaseHelper.COLUMN_CHANNEL_ROOM_ID, room);
			    NanoHTTPD.myOut.println("modified function");

		    
			homeMaticDatabase.update(DatabaseHelper.TABLE_CHANNEL, contentValues, "_id=" + id, null);
	  }

	  /**
	   * delete a position from the database
	   * @param position position to be deleted
	   * @return if deleted
	   */
	  public static void deletePosition (Position position) {
	    long id = position.get_id();			    
	    System.out.println("Position deleted with id: " + id);
	    homeMaticDatabase.delete(DatabaseHelper.TABLE_POSITION, DatabaseHelper.COLUMN_POSITION_ID
	        + " = " + id, null);
	    //NanoHTTPD.myOut.println("BATMAN");
	    
	    
	  }

	  /**
	   * returns all entries of the table as List<type>
	   * @return List<type> of all database entries
	   */
	  public static List<Position> getAllPositions() {
	    List<Position> positions = new ArrayList<Position>();
	    
	    Cursor cursor = homeMaticDatabase.query(DatabaseHelper.TABLE_POSITION,
	        allColumnsPosition, null, null, null, null, null);
	 
	    cursor.moveToFirst();
	    while (!cursor.isAfterLast()) {
	      Position position = cursorToPosition(cursor);
	      positions.add(position);
	      cursor.moveToNext();
	    }
	    cursor.close();
	    return positions;
	  }

	  /**
	   * turns database cursor into object
	   * @param cursor cursor to database entry which should be returned into an object
	   * @return object containing the database data
	   */
	  private static Position cursorToPosition(Cursor cursor) {
	    Position position = new Position();
	    position.set_id(cursor.getLong(0));
	    position.setChannel_id(cursor.getString(1));
	    position.setX_coord(cursor.getLong(2));
	    position.setY_coord(cursor.getLong(3));
	    position.setBuffer(cursor.getLong(4));
	    return position;
	  }
	  
	  /**
	   * returns all entries of the table as List<type>
	   * @return List<type> of all database entries
	   */
	  public static List<Value> getMyValues(Channel channel) {
		  List<Value> values = new ArrayList<Value>();
		  Cursor cursor = homeMaticDatabase.query(DatabaseHelper.TABLE_VALUE,
		            allColumnsValue, DatabaseHelper.COLUMN_VALUE_CHANNEL_ID + " = " + '"' + channel.get_id() + '"', null,
		            null, null, null);
		    cursor.moveToFirst();
		    while (!cursor.isAfterLast()) {
		      Value value = cursorToValue(cursor);
		      values.add(value);
		      cursor.moveToNext();
		      //NanoHTTPD.myOut.println(value.getName());
		    }
		    return values;
	  }
		
	  /**
	   * returns all entries of the table as List<type>
	   * @return List<type> of all database entries
	   */
	  public static List<Value> getAllValues() {
		    List<Value> values = new ArrayList<Value>();
		    
		    Cursor cursor = homeMaticDatabase.query(DatabaseHelper.TABLE_VALUE,
		        allColumnsValue, null, null, null, null, null);
		 
		    cursor.moveToFirst();
		    while (!cursor.isAfterLast()) {
		      Value value = cursorToValue(cursor);
		      values.add(value);
		      cursor.moveToNext();
		    }
		    cursor.close();
		    return values;
		  }
}
  
 