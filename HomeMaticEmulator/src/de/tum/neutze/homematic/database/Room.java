package de.tum.neutze.homematic.database;

/**
* @author Johannes Neutze
* @version 1.0
* @since 1.6
*/

public class Room {
	long _id;
	String name;
	
	/**
	 * getters for the class
	 * sets values for the object
	 * @return requested parameter
	 */
	public long get_id() {
		return _id;
	}
	public String getName() {
		return name;
	}
	
	/**
	 * setters for the class
	 * get values from the object
	 * @param parameter to set
	 */
	public void set_id(long _id) {
		this._id = _id;
	}
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * returns the string for the class
	 * @return string name
	 */
	public String toString() {
		return name;
	}
}
