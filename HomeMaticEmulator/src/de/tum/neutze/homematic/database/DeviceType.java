package de.tum.neutze.homematic.database;

/**
* @author Johannes Neutze
* @version 1.0
* @since 1.6
*/

public class DeviceType {
	long _id;
	String forget_it;
	String hw;
	long cnt;
	String channel_type;
	
	/**
	 * getters for the class
	 * sets values for the object
	 * @return requested parameter
	 */
	public long get_id() {
		return _id;
	}
	public String getForget_it() {
		return forget_it;
	}
	public String getHw() {
		return hw;
	}
	public long getCnt() {
		return cnt;
	}
	public String getChannel_type() {
		return channel_type;
	}
	
	/**
	 * setters for the class
	 * get values from the object
	 * @param parameter to set
	 */
	public void set_id(long _id) {
		this._id = _id;
	}
	public void setForgetIt(String forget_it) {
		this.forget_it = forget_it;
	}
	public void setHw(String hw) {
		this.hw = hw;
	}
	public void setCnt(long cnt) {
		this.cnt = cnt;
	}
	public void setChannel_type(String channel_type) {
		this.channel_type = channel_type;
	}
}
