package de.tum.neutze.homematic.database;

import java.util.List;

/**
* @author Johannes Neutze
* @version 1.0
* @since 1.6
*/

public class Device {
	long _id;
	String name;
	String address;
	String device_interface;
	String type;
	
	/**
	 * getters for the class
	 * sets values for the object
	 * @return requested parameter
	 */
	public long get_id() {
		return _id;
	}
	public String getName() {
		return name;
	}
	public String getAddress() {
		return address;
	}
	public String getDevice_interface() {
		return device_interface;
	}
	public String getType() {
		return type;
	}
	
	/**
	 * setters for the class
	 * get values from the object
	 * @param parameter to set
	 */
	public void set_id(long _id) {
		this._id = _id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public void setDevice_interface(String device_interface) {
		this.device_interface = device_interface;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	/**
	 * returns the string for the class
	 * @return concatenated string of name and device type
	 */
	public String toString() {
		String typeName = "error";
		List<HWDevice> hwdevices = DataSource.getAllHWDevices();
    	String[] devices = new String[(hwdevices.size()+1)];
    	
    	//if no device is existing
    	devices[0] = "select a type for the new device";
    	
    	//searches for matching device type
    	for (int i = 0; i < (hwdevices.size()); i++) {
    		if (type.equals(hwdevices.get(i).getHw())) {
    			typeName = hwdevices.get(i).getType();
    		}
    	}
		return name + " - " + typeName;
	}
}
