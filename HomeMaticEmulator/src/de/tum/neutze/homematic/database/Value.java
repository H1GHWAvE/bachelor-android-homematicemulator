package de.tum.neutze.homematic.database;

/**
* @author Johannes Neutze
* @version 1.0
* @since 1.6
*/

public class Value {
	long _id;
	String name;
	String value_type;
	String state;
	long timestamp;
	long channel_id;
	long dp_id;
	
	/**
	 * getters for the class
	 * sets values for the object
	 * @return requested parameter
	 */
	public long get_id() {
		return _id;
	}
	public String getName() {
		return name;
	}
	public String getValue_type() {
		return value_type;
	}
	public String getState() {
		return state;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public long getChannel_id() {
		return channel_id;
	}
	public long getDp_id() {
		return dp_id;
	}
	
	/**
	 * setters for the class
	 * get values from the object
	 * @param parameter to set
	 */
	public void set_id(long _id) {
		this._id = _id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setValue_type(String value_type) {
		this.value_type = value_type;
	}
	public void setState(String state) {
		this.state = state;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public void setChannel_id(long channel_id) {
		this.channel_id = channel_id;
	}
	public void setDp_id(long dp_id) {
		this.dp_id = dp_id;
	}
}
