package de.tum.neutze.homematic.database;

/**
* @author Johannes Neutze
* @version 1.0
* @since 1.6
*/

public class HWDevice {
	long _id;
	String type;
	String hw;

	/**
	 * getters for the class
	 * sets values for the object
	 * @return requested parameter
	 */
	public long get_id() {
		return _id;
	}
	public String getType() {
		return type;
	}
	public String getHw() {
		return hw;
	}
	
	/**
	 * setters for the class
	 * get values from the object
	 * @param parameter to set
	 */
	public void set_id(long _id) {
		this._id = _id;
	}
	public void setType(String type) {
		this.type = type;
	}
	public void setHw(String hw) {
		this.hw = hw;
	}

}
