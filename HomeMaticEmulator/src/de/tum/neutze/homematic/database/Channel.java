package de.tum.neutze.homematic.database;

import java.util.List;

/**
* @author Johannes Neutze
* @version 1.0
* @since 1.6
*/

public class Channel {
	long _id;
	String name;
	String address;
	long parent_device;
	long device_index;
	long function_id;
	long room_id;
	
	/**
	 * getters for the class
	 * sets values for the object
	 * @return requested parameter
	 */
	public long get_id() {
		return _id;
	}
	public String getName() {
		return name;
	}
	public String getAddress() {
		return address;
	}
	public long getParent_device() {
		return parent_device;
	}
	public long getDevice_index() {
		return device_index;
	}
	public long getFunction_id() {
		return function_id;
	}
	public long getRoom_id() {
		return room_id;
	}
	
	/**
	 * setters for the class
	 * get values from the object
	 * @param parameter to set
	 */
	public void set_id(long _id) {
		this._id = _id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public void setParent_device(long parent_device) {
		this.parent_device = parent_device;
	}
	public void setDevice_index(long device_index) {
		this.device_index = device_index;
	}
	public void setFunction_id(long function_id) {
		this.function_id = function_id;
	}
	public void setRoom_id(long room_id) {
		this.room_id = room_id;
	}
	
	/**
	 * returns the string for the class
	 * @return concatenated string of name, device, room and function
	 */
	
	public String toString() {
		String device = "";
		String room = " ( no room // ";
		String function = "no function)";
		
		//searches the the parent device id
		List<Device> devices = DataSource.getAllDevices();
		for (int i = 0; i < devices.size(); i++) {
			if (parent_device == devices.get(i).get_id()) {
				device = " - " + devices.get(i).getName();
			}
		}
		
		//searches the matching room name
		List<Room> rooms = DataSource.getAllRooms();
		for (int j = 0; j < rooms.size(); j++) {
			if (room_id == rooms.get(j).get_id()) {
				room = " ( " + rooms.get(j).getName() + " // ";
			}
		}
		
		//searches the matching function name
		List<Function> functions = DataSource.getAllFunctions();
		for (int k = 0; k < functions.size(); k++) {
			if (function_id == functions.get(k).get_id()) {
				function = functions.get(k).getName() + ")";
			}
		}
		return name + device + room + function;
	}
}
