package de.tum.neutze.homematic.apihelper;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import de.tum.neutze.homematic.database.Channel;
import de.tum.neutze.homematic.database.DataSource;
import de.tum.neutze.homematic.database.Device;
import de.tum.neutze.homematic.database.Function;
import de.tum.neutze.homematic.database.Room;
import de.tum.neutze.homematic.database.Value;
import de.tum.neutze.homematic.server.NanoHTTPD;
import de.tum.neutze.homematic.server.NanoHTTPD.Response;
import de.tum.neutze.homematic.activities.MainActivity;

import android.database.Cursor;

/**
* @author Johannes Neutze
* @version 1.0
* @since 1.6
*/

public class ListHelper {
	/**
	 * values containing 1337 aren't documented or not needed by HomeDroid
	 * they are populated with the 1337 dummy to be easily recognizable
	 */
	Response res;
	//standard XML header
	static String xmlHeader = "<?xml version="+'"'+"1.0"+'"'+"encoding="+'"'+"ISO-8859-1"+'"'+" ?>";
	
	/**
	 * creates the deviceList for the client
	 * @return deviceList
	 */
	public static String deviceList() {
		String xmlDeviceListContent = "<deviceList>";
		String xmlDeviceListContentDevice = new String();
		boolean channel_added = false;
		
		List<Device> devices = DataSource.getAllDevices();
		

		for (int i = 0; i < devices.size(); i++) {
			channel_added = false;
			xmlDeviceListContentDevice += "<device"
									+ " name=" +'"' + devices.get(i).getName() + '"'
									+ " address=" +'"' + devices.get(i).getAddress() + '"'
									+ " ise_id=" +'"' + String.valueOf(devices.get(i).get_id()) + '"'
									+ " interface=" +'"' + devices.get(i).getDevice_interface() + '"'
									
									+ " device_type=" +'"' + devices.get(i).getType() + '"' 
									+ " ready_config=" +'"' + "true" + '"' + ">";
		
		List<Channel> channels = DataSource.getAllChannels();
		for (int j = 0; j < channels.size(); j++)
		{
			if (channels.get(j).getParent_device() == devices.get(i).get_id())
			{
				xmlDeviceListContentDevice += "<channel"
										+ " name=" +'"' + channels.get(j).getName() + '"'
										+ " type=" + '"' + 1337 + '"'
										+ " address=" + '"' + channels.get(j).getAddress() + '"' 
										+ " ise_id=" + '"' + String.valueOf(channels.get(j).get_id()) + '"' 
										+ " direction=" + '"' + 1337 + '"' 
										+ " parent_device=" + '"' + channels.get(j).getParent_device() + '"' 
										+ " index=" + '"' + channels.get(j).getDevice_index() + '"' 
										+ " group_partner=" + '"' + 1337 + '"'
										+ " aes_available=" + '"' + 1337 + '"'
										+ " transmission_mode=" + '"' + 1337 + '"' 
										+ " visible=" + '"' + 1337 + '"'
										+ " ready_config=" + '"' + 1337 + '"' + "/>";
				channel_added = true; 
			}
				
			}
			if (channel_added) {
				NanoHTTPD.myOut.println(channel_added);
				xmlDeviceListContentDevice += "</device>";
				xmlDeviceListContent += xmlDeviceListContentDevice;		
			}
			xmlDeviceListContentDevice = "";
		
		}
		xmlDeviceListContent += xmlDeviceListContentDevice + "</deviceList>";		
		NanoHTTPD.myOut.println(xmlHeader + xmlDeviceListContent);
		return xmlHeader + xmlDeviceListContent;
	}
	
	/**
	 * creates the favoriteList for the client
	 * @return favoriteList
	 */
	public static String favoriteList() {
		String xml = "<favoritelist></favoritelist>";
		return xmlHeader+xml;
	}

	/**
	 * creates the functionList for the client
	 * @return functionlist
	 */
	public static String functionList() {
		String xmlFunctionListContent = "<functionList>";
		boolean channel_added = false;
		
		List<Function> functions = DataSource.getAllFunctions();
		List<Channel> channels = DataSource.getAllChannels();

		for(int i = 0; i < functions.size() ; i++) {
			
			xmlFunctionListContent += "<function name=" +'"' + functions.get(i).getName() + '"'
					+ " description=" + '"' + functions.get(i).getDescription() + '"'
					+ " ise_id=" + '"' + String.valueOf(functions.get(i).get_id()) + '"';
			channel_added = false;
			for (int j = 0; j < channels.size(); j++)
			{
				if (functions.get(i).get_id() == channels.get(j).getFunction_id())
				{
					//NanoHTTPD.myOut.println(functions.get(i).get_id() + " // " + channels.get(j).getFunction_id());

					xmlFunctionListContent +=  "><channel address=" + '"' + channels.get(j).getAddress() + '"'
							+ " ise_id=" + '"' + String.valueOf(channels.get(j).get_id()) + '"' + "/";
					channel_added = true;
				}			
			}
			if (channel_added)
				xmlFunctionListContent += "></function>";
			else
				xmlFunctionListContent += "/>";		
		}
		
		xmlFunctionListContent += "</functionList>";
		//NanoHTTPD.myOut.println(xmlHeader + xmlFunctionListContent);
		return xmlHeader + xmlFunctionListContent;
	}

	/**
	 * creates the programList for the client
	 * @return programList
	 */
	public static String programList() {
		String xml ="<programList></programList>";
		return xmlHeader+xml;
	}

	/**
	 * creates the roomList for the client
	 * @return roomList
	 */
	public static String roomList() {
		String xmlRoomListContent = "<roomList>";
		boolean channel_added = false;
		
		List<Room> rooms = DataSource.getAllRooms();
		List<Channel> channels = DataSource.getAllChannels();

		for(int i = 0; i < rooms.size() ; i++) {
			xmlRoomListContent += "<room name=" +'"' + rooms.get(i).getName() + '"'
					+ " ise_id=" + '"' + String.valueOf(rooms.get(i).get_id()) + '"';
			channel_added = false;
			for (int j = 0; j < channels.size(); j++)
			{			
				if (rooms.get(i).get_id() ==channels.get(j).getRoom_id())
				{
					xmlRoomListContent +=  "><channel ise_id=" + '"' + String.valueOf(channels.get(j).get_id()) + '"' + "/";
					channel_added = true;
				}			
			}
			if (channel_added)
				xmlRoomListContent += "></room>";
			else
				xmlRoomListContent += "/>";		
		}
		
		xmlRoomListContent += "</roomList>";
		//NanoHTTPD.myOut.println(xmlHeader + xmlRoomListContent);
		return xmlHeader + xmlRoomListContent;
	}

	/**
	 * creates the state code for the client
	 * @param parms channel id
	 * @return state of the given channel id
	 */
	public static String state(Properties parms) {
		Enumeration<?> e = parms.propertyNames();
		String value = (String)e.nextElement();
		String channel = (String) parms.getProperty( value );
		String xml = "<state>";
		List <Device> devices = DataSource.getAllDevices();
		List <Channel> channels = DataSource.getAllChannels();
		List <Value> values = DataSource.getAllValues();
		boolean exists = false;
		for (int i = 0; i < devices.size(); i++) {
			String xmlContent = "<device name=" + '"' + devices.get(i).getName() + '"'
			+ "ise_id=" + '"' + String.valueOf(devices.get(i).get_id()) + '"'
			+ ">";
			exists = false;
			for (int j = 0; j < channels.size(); j++) {
				if (devices.get(i).get_id() == channels.get(j).getParent_device() && Long.parseLong(channel) == channels.get(j).get_id()) {
					xmlContent += "<channel name=" + '"' + channels.get(j).getName() + '"'
					+ "ise_id =" + '"' + String.valueOf(channels.get(j).get_id()) + '"'
					+ ">";
					for (int k = 0; k < values.size(); k++) {
						if (channels.get(j).get_id() == values.get(k).getChannel_id()) {// && values.get(k).getName().equals("Level")) {
							xmlContent += "<datapoint name =" + '"' + channels.get(j).getName() + "." + values.get(k).getName() + '"'
							+ " type= " + '"' + values.get(k).getName() + '"'
							+ " ise_id=" + '"' + String.valueOf(values.get(k).get_id()) + '"'
							+ " value=" + '"' + values.get(k).getState() + '"'
							+ " value_type=" + '"' + values.get(k).getValue_type() + '"'
							+ " timestamp=" + '"' + values.get(k).getTimestamp() + '"'
							+ "/>";
							exists = true;
						}
					}
					xmlContent += "</channel>";

				}

			}					
			xmlContent += "</device>";
			if (exists)
			{
				xml += xmlContent;
			}
			exists = false;
		}
	
		xml += "</state>";
		NanoHTTPD.myOut.println(xmlHeader+xml);
		return xmlHeader+xml;
	}

	/**
	 * changes the channel id's value
	 * @param parms channel id and new value
	 * @return id of the changed channel
	 */
	public static String statechange(Properties parms) {

		Enumeration<?> e = parms.propertyNames();
		List<String> statechange = new ArrayList<String>();
		while ( e.hasMoreElements())
		{
			String value = (String)e.nextElement();
			statechange.add((String) parms.getProperty( value ));
		}
		List<String> change = new ArrayList<String>();
		change.add(statechange.get(1));
		DataSource.modifyValues(Long.parseLong(statechange.get(0)), change);

		String xml ="<result><changed id=" + '"' + statechange.get(0) + '"' + "/></result>";
		NanoHTTPD.myOut.println(xml);
		return xmlHeader+xml;
	}

	/**
	 * creates the stateList for the client
	 * @return stateList
	 */
	public static String stateList() {
		String xml = "<stateList>";
		List <Device> devices = DataSource.getAllDevices();
		List <Channel> channels = DataSource.getAllChannels();
		List <Value> values = DataSource.getAllValues();
		boolean exists = false;
		for (int i = 0; i < devices.size(); i++) {
				String xmlContent = "<device name=" + '"' + devices.get(i).getName() + '"'
				+ "ise_id=" + '"' + String.valueOf(devices.get(i).get_id()) + '"'
				+ ">";
				exists = false;
				for (int j = 0; j < channels.size(); j++) {
					if (devices.get(i).get_id() == channels.get(j).getParent_device()) {
						xmlContent += "<channel name=" + '"' + channels.get(j).getName() + '"'
						+ "ise_id =" + '"' + String.valueOf(channels.get(j).get_id()) + '"'
						+ ">";
						for (int k = 0; k < values.size(); k++) {
							if (channels.get(j).get_id() == values.get(k).getChannel_id()) {// && values.get(k).getName().equals("Level")) {
								xmlContent += "<datapoint name =" + '"' + channels.get(j).getName() + "." + values.get(k).getName() + '"'
								+ " type= " + '"' + values.get(k).getName() + '"'
								+ " ise_id=" + '"' + String.valueOf(values.get(k).get_id()) + '"'
								+ " value=" + '"' + values.get(k).getState() + '"'
								+ " value_type=" + '"' + values.get(k).getValue_type() + '"'
								+ " timestamp=" + '"' + values.get(k).getTimestamp() + '"'
								+ "/>";
								exists = true;
							}
						}
						xmlContent += "</channel>";

					}

				}					
				xmlContent += "</device>";
				if (exists)
				{
					xml += xmlContent;
				}
				exists = false;
		}
			xml += "</stateList>";
		NanoHTTPD.myOut.println(xmlHeader+xml);
		return xmlHeader+xml;
	}

	/**
	 * creates the sysvarList for the client
	 * @return sysvarList
	 */
	public static String sysvarList() {
		String xml = "<systemVariables></systemVariables>";
		return xmlHeader+xml;
	}

	/**
	 * creates the version code for the client
	 * @return version number
	 */
	public static String version() {
		String xml = "<version>1.2</version>";
		return xmlHeader+xml;
	}
	
	public static final String
	devicelist = "devicelist.cgi",
	favoritelist = "favoritelist.cgi",
	functionlist = "functionlist.cgi",
	programlist = "programlist.cgi",
	roomlist = "roomlist.cgi",
	state = "state.cgi",
	statechange = "statechange.cgi",
	statelist = "statelist.cgi",
	sysvarlist = "sysvarlist.cgi",
	version = "version.cgi";
	
	public static final String
	MIME_PLAINTEXT = "text/plain",
	MIME_HTML = "text/html",
	MIME_DEFAULT_BINARY = "application/octet-stream",
	MIME_XML = "text/xml";
	
	public static final String
	HTTP_OK = "200 OK",
	HTTP_PARTIALCONTENT = "206 Partial Content",
	HTTP_RANGE_NOT_SATISFIABLE = "416 Requested Range Not Satisfiable",
	HTTP_REDIRECT = "301 Moved Permanently",
	HTTP_NOTMODIFIED = "304 Not Modified",
	HTTP_FORBIDDEN = "403 Forbidden",
	HTTP_NOTFOUND = "404 Not Found",
	HTTP_BADREQUEST = "400 Bad Request",
	HTTP_INTERNALERROR = "500 Internal Server Error",
	HTTP_NOTIMPLEMENTED = "501 Not Implemented";
	
}
