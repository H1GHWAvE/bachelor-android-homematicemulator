package de.tum.neutze.homematic.activities;

import java.io.File;
import java.io.IOException;

import de.tum.neutze.homematic.database.DataSource;
import de.tum.neutze.homematic.server.NanoHTTPD;

import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.MediaStore;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

/**
* @author Johannes Neutze
* @version 1.0
* @since 1.6
*
*/

public class MainActivity extends Activity {
	
	private static final int PORT = 1337;
	private MyHTTPD server;
	public static DataSource datasource;

	/**
	 * called upon activity creation
	 * creates the interface dynamically
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		RoomDrawer d = new RoomDrawer(this);
		setContentView(d);
		
		datasource = new DataSource(this);
	    datasource.open();
	    
	    //starts the server
	    try {
	        server = new MyHTTPD();
	      } catch (IOException e) {
	        e.printStackTrace();
	      }
		}

	/**
	 * refreshs the background on resume
	 */
	@Override
	  protected void onResume() {
	    super.onResume();
	    RoomDrawer d = new RoomDrawer(this);
	    setContentView(d);
	  }

	/**
	 * not used
	 */
	  protected void onClose() {
	    super.onPause();
	  }
	 
	 /**
	  * creates the options menu 
	  */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	/**
	 * handles actions for option items being selected
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
    	Intent intent = new Intent(this, Create.class);
	    switch (item.getItemId()) {
	        case R.id.menu_device:
                intent.putExtra("type", "device");
                startActivity(intent);
				this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	            return true;
	        case R.id.menu_room:
                intent.putExtra("type", "room");
                startActivity(intent);
				this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
				return true;
	        case R.id.menu_function:
                intent.putExtra("type", "function");
                startActivity(intent);
				this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
				return true;
	        case R.id.menu_ip:
	            WifiManager myWifiManager = (WifiManager)getSystemService(getApplicationContext().WIFI_SERVICE);
	            
	            WifiInfo myWifiInfo = myWifiManager.getConnectionInfo();
	            int myIp = myWifiInfo.getIpAddress();
	           
	            int intMyIp3 = myIp/0x1000000;
	            int intMyIp3mod = myIp%0x1000000;
	           
	            int intMyIp2 = intMyIp3mod/0x10000;
	            int intMyIp2mod = intMyIp3mod%0x10000;
	           
	            int intMyIp1 = intMyIp2mod/0x100;
	            int intMyIp0 = intMyIp2mod%0x100;
	           
	            String textIp =String.valueOf(intMyIp0)
	              + "." + String.valueOf(intMyIp1)
	              + "." + String.valueOf(intMyIp2)
	              + "." + String.valueOf(intMyIp3);
	        	Toast.makeText(getApplicationContext(), "Connect to " + textIp + ":" + PORT, Toast.LENGTH_LONG).show();
	        	return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}

	/**
	 * class to start the nanoHTTPD
	 * @author Johannes Neutze
	 *
	 */
	private class MyHTTPD extends NanoHTTPD {
	    public MyHTTPD() throws IOException {
	      super(PORT, new  File("/sdcard/Download"));
	    }
	 }
}
