package de.tum.neutze.homematic.activities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.tum.neutze.homematic.database.Channel;
import de.tum.neutze.homematic.database.DataPoint;
import de.tum.neutze.homematic.database.DataSource;
import de.tum.neutze.homematic.database.Device;
import de.tum.neutze.homematic.database.Function;
import de.tum.neutze.homematic.database.Position;
import de.tum.neutze.homematic.database.Room;
import de.tum.neutze.homematic.database.Value;
import de.tum.neutze.homematic.server.NanoHTTPD;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/**
* @author Johannes Neutze
* @version 1.0
* @since 1.6
*/

public class ChannelEditor extends Activity{
	String position_id;
	Spinner room_spinner;
	Spinner channel_spinner;
	Spinner function_spinner;
	EditText editValue1;
	EditText editValue2;
	EditText editValue3;
	EditText editValue4;
	EditText editValue5;
	EditText editValue6;
	EditText editValue7;
	EditText editValue8;
	EditText editValue9;
	EditText editValue10;
	EditText editValue11;
	EditText editValue12;
	EditText editValue13;
	TextView value1;
	TextView value2;
	TextView value3;
	TextView value4;
	TextView value5;
	TextView value6;
	TextView value7;
	TextView value8;
	TextView value9;
	TextView value10;
	TextView value11;
	TextView value12;
	TextView value13;
	Channel currentChannel;
	Room currentRoom;
	Function currentFunction;
	Position currentPosition;
	
	/**
	 * called upon activity creation
	 * creates the interface dynamically
	 */
	  @Override
	  public void onCreate(Bundle savedInstanceState) {
		  super.onCreate(savedInstanceState);
		    setContentView(R.layout.activity_channeleditor);
		    Bundle extras = getIntent().getExtras();
		    position_id = extras.getString("position_id");
		    
		    //create and hide value fields
	    	Spinner room_spinner = (Spinner) findViewById(R.id.rooms);
	    	Spinner function_spinner = (Spinner) findViewById(R.id.functions);		 
		    value1 = (TextView) findViewById(R.id.value1);
		    value1.setVisibility(View.GONE);
		    editValue1 = (EditText) findViewById(R.id.editValue1);
		    editValue1.setVisibility(View.GONE);
		    value2 = (TextView) findViewById(R.id.value2);
		    value2.setVisibility(View.GONE);
		    editValue2 = (EditText) findViewById(R.id.editValue2);
		    editValue2.setVisibility(View.GONE);
		    value3 = (TextView) findViewById(R.id.value3);
		    value3.setVisibility(View.GONE);
		    editValue3 = (EditText) findViewById(R.id.editValue3);
		    editValue3.setVisibility(View.GONE);
		    value4 = (TextView) findViewById(R.id.value4);
		    value4.setVisibility(View.GONE);
		    editValue4 = (EditText) findViewById(R.id.editValue4);
		    editValue4.setVisibility(View.GONE);
		    value5 = (TextView) findViewById(R.id.value5);
		    value5.setVisibility(View.GONE);
		    editValue5 = (EditText) findViewById(R.id.editValue5);
		    editValue5.setVisibility(View.GONE);
		    value6 = (TextView) findViewById(R.id.value6);
		    value6.setVisibility(View.GONE);
		    editValue6 = (EditText) findViewById(R.id.editValue6);
		    editValue6.setVisibility(View.GONE);
		    value7 = (TextView) findViewById(R.id.value7);
		    value7.setVisibility(View.GONE);
		    editValue7 = (EditText) findViewById(R.id.editValue7);
		    editValue7.setVisibility(View.GONE);
		    value8 = (TextView) findViewById(R.id.value8);
		    value8.setVisibility(View.GONE);
		    editValue8 = (EditText) findViewById(R.id.editValue8);
		    editValue8.setVisibility(View.GONE);
		    value9 = (TextView) findViewById(R.id.value9);
		    value9.setVisibility(View.GONE);
		    editValue9 = (EditText) findViewById(R.id.editValue9);
		    editValue9.setVisibility(View.GONE);
		    value10 = (TextView) findViewById(R.id.value10);
		    value10.setVisibility(View.GONE);
		    editValue10 = (EditText) findViewById(R.id.editValue10);
		    editValue10.setVisibility(View.GONE);
		    value11 = (TextView) findViewById(R.id.value11);
		    value11.setVisibility(View.GONE);
		    editValue11 = (EditText) findViewById(R.id.editValue11);
		    editValue11.setVisibility(View.GONE);
		    value12 = (TextView) findViewById(R.id.value12);
		    value12.setVisibility(View.GONE);
		    editValue12 = (EditText) findViewById(R.id.editValue12);
		    editValue12.setVisibility(View.GONE);
		    value13 = (TextView) findViewById(R.id.value13);
		    value13.setVisibility(View.GONE);
		    editValue13 = (EditText) findViewById(R.id.editValue13);
		    editValue13.setVisibility(View.GONE);
		    
		    List<Position> positions = DataSource.getAllPositions();
		    List<Channel> channels = DataSource.getAllChannels();
		    List<Device> devices = DataSource.getAllDevices();
		    for (int i = 0; i < positions.size(); i++) {
		    	//NanoHTTPD.myOut.println(position_id + " // " + positions.get(i).get_id());
		    	if (position_id.equals(String.valueOf(positions.get(i).get_id()))) {
		    		currentPosition = positions.get(i);
		    	}
		    }

		    for (int k = 0; k < channels.size(); k++) {
		    	//NanoHTTPD.myOut.println(currentPosition.getChannel_id() + " // " + channels.get(k).get_id());
		    	if (currentPosition.getChannel_id().equals(String.valueOf(channels.get(k).get_id()))) {
		    		currentChannel = channels.get(k);
		    	}
		    }
		    
	    	//room spinner
		    List<Room> rooms = DataSource.getAllRooms();

	    	String[] roomsArray = new String[(rooms.size())];
	    	for (int l = 0; l < (rooms.size()); l++) {
	    		roomsArray[l] = rooms.get(l).getName();	    		
	    	}
	    	Arrays.sort(roomsArray);	
	    	ArrayAdapter<String> room_adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, roomsArray);
	    	room_spinner.setAdapter(room_adapter);
	    	
	    	//function spinner
	    	List<Function> functions = DataSource.getAllFunctions();
	    	String[] functionsArray = new String[(functions.size()+1)];
	    	functionsArray[0] = "  ";
	    	for (int l = 1; l < (functions.size()+1); l++) {
	    		functionsArray[l] = functions.get(l-1).getName();	    		
	    	}
	    	Arrays.sort(functionsArray);
	    	functionsArray[0] = "select a function";
	    	if (functionsArray.length==1) {
	    		functionsArray[0] = "no functions available";
	    	}
	    	ArrayAdapter<String> function_adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, functionsArray);
	    	function_spinner.setAdapter(function_adapter);
			

	    	//preset spinner
	    	// set room
	    	for (int j = 0; j < rooms.size(); j++) {
	    		if (currentChannel.getRoom_id() == rooms.get(j).get_id()) {  			
	    			currentRoom = rooms.get(j);
	    		}
	    	}
	    	if (currentRoom !=null) {
		    	int roomPosition = room_adapter.getPosition(currentRoom.getName());
		    	room_spinner.setSelection(roomPosition);
	    	}
	    	//set function
	    	for (int j = 0; j < functions.size(); j++) {
	    		if (currentChannel.getFunction_id() == functions.get(j).get_id()) {  			
	    			currentFunction = functions.get(j);
	    		}
	    	}
	    	if (currentFunction!=null) {
		    	int functionPosition = function_adapter.getPosition(currentFunction.getName());
		    	function_spinner.setSelection(functionPosition);
	    	}
	    	
		    EditText setChannel = (EditText) findViewById(R.id.channelName);
		    TextView setDevice = (TextView) findViewById(R.id.deviceName);
		    TextView setId = (TextView) findViewById(R.id.device);
		    setId.setText("Device ID: " + String.valueOf(currentChannel.get_id()) + "");
		    //set name
		    setChannel.setText(currentChannel.getName());
		    //set device
		    for (int ii = 0; ii < devices.size(); ii++) {
		    	if(currentChannel.getParent_device() == devices.get(ii).get_id()) {
				    setDevice.setText("   " + devices.get(ii).getName());
				    
		    	}
		    }

		    List<Value> datapoints = DataSource.getMyValues(currentChannel);
		    
		    NanoHTTPD.myOut.println(datapoints.size());

		    //create value fields if not empty
		    if (0 < datapoints.size()) {
			    value1.setVisibility(View.VISIBLE);
			    value1.setText(datapoints.get(0).getName());
			    editValue1.setVisibility(View.VISIBLE);
			    editValue1.setText(datapoints.get(0).getState());
		    }
		    if (1 < datapoints.size()) {
			    value2.setVisibility(View.VISIBLE);
			    value2.setText(datapoints.get(1).getName());
			    editValue2.setVisibility(View.VISIBLE);
			    editValue2.setText(datapoints.get(1).getState());
		    }
		    if (2 < datapoints.size()) {
			    value3.setVisibility(View.VISIBLE);
			    value3.setText(datapoints.get(2).getName());
			    editValue3.setVisibility(View.VISIBLE);
			    editValue3.setText(datapoints.get(2).getState());
		    }
		    if (3 < datapoints.size()) {
			    value4.setVisibility(View.VISIBLE);
			    value4.setText(datapoints.get(3).getName());
			    editValue4.setVisibility(View.VISIBLE);
			    editValue4.setText(datapoints.get(3).getState());		    
			    }
		    if (4 < datapoints.size()) {
			    value5.setVisibility(View.VISIBLE);
			    value5.setText(datapoints.get(4).getName());
			    editValue5.setVisibility(View.VISIBLE);
			    editValue5.setText(datapoints.get(4).getState());
		    }
		    if (5 < datapoints.size()) {
			    value6.setVisibility(View.VISIBLE);
			    value6.setText(datapoints.get(5).getName());
			    editValue6.setVisibility(View.VISIBLE);
			    editValue6.setText(datapoints.get(5).getState());
		    }
		    if (6 < datapoints.size()) {
			    value7.setVisibility(View.VISIBLE);
			    value7.setText(datapoints.get(6).getName());
			    editValue7.setVisibility(View.VISIBLE);
			    editValue7.setText(datapoints.get(6).getState());
		    }
		    if (7 < datapoints.size()) {
			    value8.setVisibility(View.VISIBLE);
			    value8.setText(datapoints.get(7).getName());
			    editValue8.setVisibility(View.VISIBLE);
			    editValue8.setText(datapoints.get(7).getState());
		    }
		    if (8 < datapoints.size()) {
			    value9.setVisibility(View.VISIBLE);
			    value9.setText(datapoints.get(8).getName());
			    editValue9.setVisibility(View.VISIBLE);
			    editValue9.setText(datapoints.get(8).getState());
		    }
		    if (9 < datapoints.size()) {
			    value10.setVisibility(View.VISIBLE);
			    value10.setText(datapoints.get(9).getName());
			    editValue10.setVisibility(View.VISIBLE);
			    editValue10.setText(datapoints.get(9).getState());
		    }
		    if (10 < datapoints.size()) {
			    value11.setVisibility(View.VISIBLE);
			    value11.setText(datapoints.get(10).getName());
			    editValue11.setVisibility(View.VISIBLE);
			    editValue11.setText(datapoints.get(10).getState());
		    }
		    if (11 < datapoints.size()) {
			    value12.setVisibility(View.VISIBLE);
			    value12.setText(datapoints.get(11).getName());
			    editValue12.setVisibility(View.VISIBLE);
			    editValue12.setText(datapoints.get(11).getState());
		    }
		    if (12 < datapoints.size()) {
			    value13.setVisibility(View.VISIBLE);
			    value13.setText(datapoints.get(12).getName());
			    editValue13.setVisibility(View.VISIBLE);
			    editValue13.setText(datapoints.get(12).getState());
		    }
		    
	  }
	  
	  /**
	   * handles the clicks on the different buttons
	   * @param view current view
	   */
	  public void onClick(View view) {
	    	Spinner room_spinner = (Spinner) findViewById(R.id.rooms);
	    	Spinner function_spinner = (Spinner) findViewById(R.id.functions);
	    	TextView device = (TextView) findViewById(R.id.device);
			String roomname =room_spinner.getSelectedItem().toString();
			String functionname =function_spinner.getSelectedItem().toString();
			String value1toString = editValue1.getText().toString();
			String value2toString = editValue2.getText().toString();
			String value3toString = editValue3.getText().toString();
			String value4toString = editValue4.getText().toString();
			String value5toString = editValue5.getText().toString();
			String value6toString = editValue6.getText().toString();
			String value7toString = editValue7.getText().toString();
			String value8toString = editValue8.getText().toString();
			String value9toString = editValue9.getText().toString();
			String value10toString = editValue10.getText().toString();
			String value11toString = editValue11.getText().toString();
			String value12toString = editValue12.getText().toString();
			String value13toString = editValue13.getText().toString();
			String value1toName = value1.getText().toString();
			String value2toName = value2.getText().toString();
			String value3toName = value3.getText().toString();
			String value4toName = value4.getText().toString();
			String value5toName = value5.getText().toString();
			String value6toName = value6.getText().toString();
			String value7toName = value7.getText().toString();
			String value8toName = value8.getText().toString();
			String value9toName = value9.getText().toString();
			String value10toName = value10.getText().toString();
			String value11toName = value11.getText().toString();
			String value12toName = value12.getText().toString();
			String value13toName = value13.getText().toString();
			
			//extract device id
			String channelId = device.getText().toString();
	    	channelId = channelId.substring(channelId.indexOf(": ")+2, channelId.length());
	    	long id = Long.parseLong(channelId);

	    	
			
		  
		    switch (view.getId()) {
		    case R.id.backCE:
		    	this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
		    	ChannelEditor.this.finish();
		    	break;
		    case R.id.deleteChannelCE:
		    	//delete assignment
		    	List<Position> positions = DataSource.getAllPositions();
		    	for (int i = 0; i < positions.size(); i++) {
		    		if(channelId.equals(positions.get(i).getChannel_id())) {
		    			DataSource.deletePosition(positions.get(i));
		    		}
		    	}
		    	this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
		    	ChannelEditor.this.finish();
		    	break;
		    case R.id.editChannelCE:
		    	List<String> values = new ArrayList<String>();
		    	List<String> names = new ArrayList<String>();
		    	//check if value was specified
		    	if (!(value1toString.equals(""))) values.add(value1toString);
		    	if (!(value2toString.equals(""))) values.add(value2toString);
		    	if (!(value3toString.equals(""))) values.add(value3toString);
		    	if (!(value4toString.equals(""))) values.add(value4toString);
		    	if (!(value5toString.equals(""))) values.add(value5toString);
		    	if (!(value6toString.equals(""))) values.add(value6toString);
		    	if (!(value7toString.equals(""))) values.add(value7toString);
		    	if (!(value8toString.equals(""))) values.add(value8toString);
		    	if (!(value9toString.equals(""))) values.add(value9toString);
		    	if (!(value10toString.equals(""))) values.add(value10toString);
		    	if (!(value11toString.equals(""))) values.add(value11toString);
		    	if (!(value12toString.equals(""))) values.add(value12toString);
		    	if (!(value13toString.equals(""))) values.add(value13toString);
		    	if (!(value1toName.equals(""))) names.add(value1toName);
		    	if (!(value2toName.equals(""))) names.add(value2toName);
		    	if (!(value3toName.equals(""))) names.add(value3toName);
		    	if (!(value4toName.equals(""))) names.add(value4toName);
		    	if (!(value5toName.equals(""))) names.add(value5toName);
		    	if (!(value6toName.equals(""))) names.add(value6toName);
		    	if (!(value7toName.equals(""))) names.add(value7toName);
		    	if (!(value8toName.equals(""))) names.add(value8toName);
		    	if (!(value9toName.equals(""))) names.add(value9toName);
		    	if (!(value10toName.equals(""))) names.add(value10toName);
		    	if (!(value11toName.equals(""))) names.add(value11toName);
		    	if (!(value12toName.equals(""))) names.add(value12toName);
		    	if (!(value13toName.equals(""))) names.add(value13toName);
		    	List<DataPoint> datapoints = DataSource.getAllDP();
		    	boolean error = false;
		    	
		    	//add each used value field
		    	for (int i = 0; i < names.size(); i++) {
		    		for (int j =0; j < datapoints.size(); j++) {
		    			//get datapoint
		    			if (names.get(i).equals(datapoints.get(j).getName())) {
		    				NanoHTTPD.myOut.println(datapoints.get(j).getType());
		    				NanoHTTPD.myOut.println(datapoints.get(j).getMin());
		    				NanoHTTPD.myOut.println(datapoints.get(j).getMax());
		    				if (datapoints.get(j).getType().equals("boolean")) {
		    					if (!(values.get(i).equals("true") || values.get(i).equals("false"))) {
		    						Toast.makeText(getApplicationContext(), names.get(i) + " must be false or true!", Toast.LENGTH_SHORT).show();
		    						error = true;
		    						break;
		    					}
		    				}
		    				//check if valuetype is correct
		    				if (datapoints.get(j).getType().equals("integer")) {

		    					if (!(values.get(i).matches("[1-9]*"))) {
		    						try {
			    						   Integer.parseInt(values.get(i));
			    						   if ((Integer.parseInt(values.get(i)) < Integer.parseInt(datapoints.get(j).getMin()) || (Integer.parseInt(values.get(i)) < Integer.parseInt(datapoints.get(j).getMin())))) {
			    							   Toast.makeText(getApplicationContext(), names.get(i) + " must be an integer value!", Toast.LENGTH_SHORT).show();
					    						error = true;
					    						break;	
			    						   }
			    						}
		    						catch (NumberFormatException nfe) {
		    							Toast.makeText(getApplicationContext(), names.get(i) + " must be an integer value!", Toast.LENGTH_SHORT).show();
			    						error = true;
			    						break;	
			    					}
		    					}
		    				}
		    				if (datapoints.get(j).getType().equals("float")) {
		    					try {
		    						   Float.parseFloat(values.get(i));
		    						}
	    						catch (NumberFormatException nfe) {
	    							Toast.makeText(getApplicationContext(), names.get(i) + " must be an float value!", Toast.LENGTH_SHORT).show();
		    						error = true;
		    						break;	
		    					}
		    				}
		    			}
		    		}
		    	}
		    	if (error) { break; }
		    	//get room
		    	List<Room> rooms = DataSource.getAllRooms();
		    	String room = "0";
		    	String function = "0";
		    	for (int k = 0; k < rooms.size(); k++) {
					if(roomname.equals(rooms.get(k).getName())) {
						room = String.valueOf(rooms.get(k).get_id());
					}
				}
		    	//get function
		    	List<Function> functions = DataSource.getAllFunctions();
				for (int l = 0; l < functions.size(); l++) {
					if(functionname.equals(functions.get(l).getName())) {
						function = String.valueOf(functions.get(l).get_id());
					}
				}
		    	DataSource.modifyValues(id, values);
	    		DataSource.modifyChannel(id, room, function);
	    		this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	    		ChannelEditor.this.finish();
		    	break;
		    
	    }
	  }
}
	  

