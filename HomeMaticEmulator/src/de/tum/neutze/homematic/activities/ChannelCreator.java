package de.tum.neutze.homematic.activities;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import de.tum.neutze.homematic.database.Channel;
import de.tum.neutze.homematic.database.DataSource;
import de.tum.neutze.homematic.database.DatabaseHelper;
import de.tum.neutze.homematic.database.Device;
import de.tum.neutze.homematic.database.Function;
import de.tum.neutze.homematic.database.HWDevice;
import de.tum.neutze.homematic.database.Position;
import de.tum.neutze.homematic.database.Room;
import de.tum.neutze.homematic.server.NanoHTTPD;
import android.os.Bundle;
import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.view.MenuInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

/**
* @author Johannes Neutze
* @version 1.0
* @since 1.6
*/

public class ChannelCreator extends ListActivity implements OnItemSelectedListener{

	Spinner room_spinner;
	Spinner channel_spinner;
	Spinner function_spinner;
	String x_coord, y_coord;
	int size = 50;

	/**
	 * called upon activity creation
	 * creates the interface dynamically
	 */
	  @Override
	  public void onCreate(Bundle savedInstanceState) {
		  super.onCreate(savedInstanceState);
		    setContentView(R.layout.activity_channelcreator);
		    
		    //coordinates
		    Bundle extras = getIntent().getExtras();
		    x_coord = extras.getString("x_coord");
		    y_coord = extras.getString("y_coord");

	    	List<Channel> channels = DataSource.getAllChannels();
	    	List<Position> positions = DataSource.getAllPositions();
	    	List<Channel> usedChannels = new ArrayList<Channel>();
	    	List<Channel> freeChannels = new ArrayList<Channel>();

	    	//check channels if already used
	    	for (int i = 0; i <channels.size(); i++) {
	    		boolean channelUsed = false;
	    		for (int j = 0; j < positions.size(); j++) {
	    			if (String.valueOf(channels.get(i).get_id()).equals(positions.get(j).getChannel_id())){
		    			channelUsed = true;
		    		}
	    		}
	    		if (channelUsed)  {
	    			usedChannels.add(channels.get(i));
	    		}
	    		else {
	    			freeChannels.add(channels.get(i));
	    		}
	    	}
	    	
	    	List<Room> rooms = DataSource.getAllRooms();
	    	List<Function> functions = DataSource.getAllFunctions();
	    	Spinner channel_spinner = (Spinner) findViewById(R.id.channels);
	    	Spinner room_spinner = (Spinner) findViewById(R.id.rooms);
	    	Spinner function_spinner = (Spinner) findViewById(R.id.functions);
	    
	    	String[] freeChannelsArray = new String[(freeChannels.size()+1)];
	    	freeChannelsArray[0] = " ";
	    	for (int l = 1; l < (freeChannels.size()+1); l++) {
	    		freeChannelsArray[l] = freeChannels.get(l-1).getName();	    		
	    	}
	    	Arrays.sort(freeChannelsArray);
	    	freeChannelsArray[0] = "select a channel";
	    	if (freeChannelsArray.length==1) {
	    		freeChannelsArray[0] = "no channels available";
	    	}
	    	ArrayAdapter<Channel> assignedChannels_adapter = new ArrayAdapter<Channel>(this, android.R.layout.simple_list_item_1, usedChannels);
		    setListAdapter(assignedChannels_adapter);
		    

	    	ArrayAdapter<String> channel_adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, freeChannelsArray);
	    	channel_spinner.setAdapter(channel_adapter);
	    	
	    	
	    	
	    	//available rooms
	    	String[] roomsArray = new String[(rooms.size()+1)];
	    	roomsArray[0] = " ";
	    	//NanoHTTPD.myOut.println(rooms.size());
	    	for (int l = 1; l < (rooms.size()+1); l++) {
	    		roomsArray[l] = rooms.get(l-1).getName();	    		
	    	}
	    	Arrays.sort(roomsArray);
	    	roomsArray[0] = "select a room";
	    	//NanoHTTPD.myOut.println(roomsArray.length);
	    	if (roomsArray.length==1) {
	    		roomsArray[0] = "no rooms available";
	    	}
	    	ArrayAdapter<String> room_adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, roomsArray);
	    	room_spinner.setAdapter(room_adapter);
	    	
	    	
	    	
	    	//available functions
	    	String[] functionsArray = new String[(functions.size()+1)];
	    	functionsArray[0] = " ";
	    	//NanoHTTPD.myOut.println(functions.size());
	    	for (int l = 1; l < (functions.size()+1); l++) {
	    		functionsArray[l] = functions.get(l-1).getName();	    		
	    	}
	    	Arrays.sort(functionsArray);
	    	functionsArray[0] = "select a function";
	    	//NanoHTTPD.myOut.println(functionsArray.length);
	    	if (functionsArray.length==1) {
	    		functionsArray[0] = "no functions available";
	    	}
	    	ArrayAdapter<String> function_adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, functionsArray);
	    	function_spinner.setAdapter(function_adapter);
	  }

	  /**
	   * super method is used
	   */
	  @Override
	  protected void onResume() {
	    super.onResume();
	  }

	  /**
	   * super method is used
	   */
	  @Override
	  protected void onPause() {
	    super.onPause();
	  }

	  /**
	   * handles the clicks on the different buttons
	   * @param view current view
	   */
	  public void onClick(View view) {
		ArrayAdapter<String> adapter = (ArrayAdapter<String>) getListAdapter();
    	Channel channel;
	    Spinner channel_spinner = (Spinner) findViewById(R.id.channels);
		String channelname = channel_spinner.getSelectedItem().toString();
		Spinner room_spinner = (Spinner) findViewById(R.id.rooms);
		String roomname = room_spinner.getSelectedItem().toString();
		Spinner function_spinner = (Spinner) findViewById(R.id.functions);
		String functionname = function_spinner.getSelectedItem().toString();
		EditText enterName = (EditText) findViewById(R.id.name);
		String name = enterName.getText().toString();

			switch (view.getId()) {
			case R.id.backCC:
				this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
				this.finish();
				break;
			case R.id.addChannelCC:
		    	List<Channel> channels = DataSource.getAllChannels();
		    	List<Room> rooms = DataSource.getAllRooms();
		    	List<Function> functions = DataSource.getAllFunctions();
		    	Channel edit = null;
		    	String room = "0";
		    	String function = "0";

		    	//check if name is given
		    	if (name.length()!=0) {
					NanoHTTPD.myOut.println(channelname + ": " + channelname.equals("select a channel") + " // " + channelname.equals("no channels available") + "==" + (!(channelname.equals("select a free channel") || channelname.equals("no channels available"))));
					//check of channel is selected
			    	if (!(channelname.equals("select a channel") || channelname.equals("no channels available"))) {
			    		for (int i = 0; i < channels.size(); i++) {
							if((channelname).contains(channels.get(i).getName()))
							{
								edit = channels.get(i);
							}
						}
				    	//get room
						for (int k = 0; k < rooms.size(); k++) {
							if(roomname.equals(rooms.get(k).getName())) {
								room = String.valueOf(rooms.get(k).get_id());
								NanoHTTPD.myOut.println(rooms.get(k).get_id());
							}
						}

						if (room.equals("0")) {
							Toast.makeText(getApplicationContext(), "Please select a room!", Toast.LENGTH_SHORT).show();
							break;
						}
						
						//get function
						for (int l = 0; l < functions.size(); l++) {
							if(functionname.equals(functions.get(l).getName())) {
								function = String.valueOf(functions.get(l).get_id());
							}
						}

						
						DataSource.createPosition(name, edit, x_coord, y_coord, size, room, function);
						Toast.makeText(getApplicationContext(), "Channel assigned", Toast.LENGTH_SHORT).show();
						adapter.notifyDataSetChanged();
						this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
						ChannelCreator.this.finish();
						break;
						
			    	}
					
			    	else {
						Toast.makeText(getApplicationContext(), "Please select a channel!", Toast.LENGTH_SHORT).show();
			    	}

				}
					
				else {
					Toast.makeText(getApplicationContext(), "Please enter a name!", Toast.LENGTH_SHORT).show();

				}
		    	break;
			}
			
	    
	  }
	  
	  /**
	   * handles the clicks on the list items
	   */ 
	  public void onListItemClick(ListView l, View v, int position, long id) {
		Channel channel = (Channel) getListAdapter().getItem(position);
		Intent intent_modify = new Intent(this, ChannelEditor.class);
        intent_modify.putExtra("position_id", String.valueOf(channel.get_id()));
        startActivity(intent_modify);
        this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        ChannelCreator.this.finish();
		  }

	  /**
	   * not used
	   */
	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * not used
	 */
	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}
}
