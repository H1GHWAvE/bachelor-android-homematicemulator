package de.tum.neutze.homematic.activities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.tum.neutze.homematic.database.DataSource;
import de.tum.neutze.homematic.database.DatabaseHelper;
import de.tum.neutze.homematic.database.Device;
import de.tum.neutze.homematic.database.Function;
import de.tum.neutze.homematic.database.HWDevice;
import de.tum.neutze.homematic.database.Room;
import de.tum.neutze.homematic.server.NanoHTTPD;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.app.ListActivity;
import android.database.Cursor;

/**
* @author Johannes Neutze
* @version 1.0
* @since 1.6
*/

public class Create extends ListActivity {
	String type;
	
	/**
	 * called upon activity creation
	 * creates the interface dynamically
	 */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	    Bundle extras = getIntent().getExtras();
	    type = extras.getString("type");
	
	    //setup for rooms menu option
	    if(type.equals("room"))  {
		    setContentView(R.layout.activity_create_room);
		    List<Room> values = DataSource.getAllRooms();
		    String[] valuesArray = new String[values.size()];
		    for (int i = 0; i < values.size(); i++)
		    {
		    	valuesArray[i] = values.get(i).getName();
		    }	    				
		    Arrays.sort(valuesArray);    				
		    List<Room> rooms = new ArrayList<Room>();
		    for (int p = 0; p < valuesArray.length; p++) {
	    		for (int q = 0; q < valuesArray.length; q++) {
	    			if (valuesArray[p].equals(values.get(q).getName())) {
	    				rooms.add(values.get(q));
	    			}
	    		}
		    }
		    ArrayAdapter<Room> adapter = new ArrayAdapter<Room>(this, android.R.layout.simple_list_item_1, rooms);
		    setListAdapter(adapter);
	    }
	    //setup for functions menu option
	    if(type.equals("function"))  {
		    setContentView(R.layout.activity_create_function);
		    List<Function> values = DataSource.getAllFunctions();
		    String[] valuesArray = new String[values.size()];
		    for (int i = 0; i < values.size(); i++)
		    {
		    	valuesArray[i] = values.get(i).getName();
		    }	    				
		    Arrays.sort(valuesArray);    				
		    List<Function> functions = new ArrayList<Function>();
		    for (int p = 0; p < valuesArray.length; p++) {
	    		for (int q = 0; q < valuesArray.length; q++) {
	    			if (valuesArray[p].equals(values.get(q).getName())) {
	    				functions.add(values.get(q));
	    			}
	    		}
		    }
		    ArrayAdapter<Function> adapter = new ArrayAdapter<Function>(this, android.R.layout.simple_list_item_1, functions);
		    setListAdapter(adapter);
	    }
	    //setup for device menu option
	    if(type.equals("device"))  {
		    setContentView(R.layout.activity_create_device);
		    List<Device> values = DataSource.getAllDevices();
		    String[] valuesArray = new String[values.size()];
		    for (int i = 0; i < values.size(); i++)
		    {
		    	valuesArray[i] = values.get(i).getName();
		    }	    				
		    Arrays.sort(valuesArray);    				
		    List<Device> devices = new ArrayList<Device>();
		    for (int p = 0; p < valuesArray.length; p++) {
	    		for (int q = 0; q < valuesArray.length; q++) {
	    			if (valuesArray[p].equals(values.get(q).getName())) {
	    				devices.add(values.get(q));
	    			}
	    		}
		    }
		    ArrayAdapter<Device> adapter = new ArrayAdapter<Device>(this, android.R.layout.simple_list_item_1, devices);
		    setListAdapter(adapter);
	    	List<HWDevice> hwdevices = DataSource.getAllHWDevices();
	    	String[] devicesArray = new String[(hwdevices.size()+1)];
	    	devicesArray[0] = " ";
	    	for (int i = 1; i < (hwdevices.size()+1); i++) {
	    		devicesArray[i] = hwdevices.get(i-1).getType() + " (Type: " + hwdevices.get(i-1).getHw() +")";
	    	}
	    	Arrays.sort(devicesArray);
	    	devicesArray[0] = "select a type for the new device";
	    	Spinner spinner = (Spinner) findViewById(R.id.deviceSpinner);
	    	ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, devicesArray);
	    	spinner.setAdapter(spinnerArrayAdapter);
	    }
    	
		TextView title = (TextView) findViewById(R.id.title);
		EditText typeName = (EditText) findViewById(R.id.name);
	    Button add = (Button) findViewById(R.id.addC);
	    Button back = (Button) findViewById(R.id.backC);
	    TextView listTitle = (TextView) findViewById(R.id.listTitle);
	    	    
	    typeName.setHint("enter a name for the new " + type);
    	title.setText("Create a new " + type);
    	add.setText("Add " + type);
    	listTitle.setText("Already existing " + type + "s (click to delete)");
	}

	  /**
	   * handles the clicks on the different buttons
	   * @param view current view
	   */
	public void onClick(View view) {
	    EditText typeName = (EditText) findViewById(R.id.name);
	    
	    //button actions for room setup
	    if (type.equals("room")) {
			ArrayAdapter<Room> adapter = (ArrayAdapter<Room>) getListAdapter();
		    String name = typeName.getText().toString();
		    switch (view.getId()) {
		    case R.id.backC:
		    	this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
		    	Create.this.finish();
		    	break;
		    case R.id.addC:
		    	if (name.length()>0) {
			    	Room newRoom = DataSource.createRoom(name);
			    	adapter.add(newRoom);
				    adapter.notifyDataSetChanged();
				    typeName.setText("");
				    break;
			    }
		    	else {
					Toast.makeText(getApplicationContext(), "Please enter a name!", Toast.LENGTH_SHORT).show();
					break;
		    	}
		    }
	    }
	    
	  //button actions for function setup
	    if (type.equals("function")) {
	    	EditText description = (EditText) findViewById(R.id.description);
			ArrayAdapter<Function> adapter = (ArrayAdapter<Function>) getListAdapter();
		    String name = typeName.getText().toString();
		    String functionDescription = description.getText().toString();
		    switch (view.getId()) {
		    case R.id.backC:
		    	this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
		    	Create.this.finish();
		    	break;
		    case R.id.addC:
		    	if (name.length()>0) {
		    		Function newFunction = DataSource.createFunction(name, functionDescription);
			    	adapter.add(newFunction);
				    adapter.notifyDataSetChanged();
				    typeName.setText("");
				    description.setText("");
				    break;
			    }
		    	else {
					Toast.makeText(getApplicationContext(), "Please enter a name!", Toast.LENGTH_SHORT).show();
					break;
		    	}
		    }
	    }
	    
	  //button actions for device setup
	    if (type.equals("device")) {
			ArrayAdapter<Device> adapter = (ArrayAdapter<Device>) getListAdapter();
		    String name = typeName.getText().toString();
	    	Spinner spinner = (Spinner) findViewById(R.id.deviceSpinner);
		    String device = spinner.getSelectedItem().toString();
		    switch (view.getId()) {
		    case R.id.backC:
		    	this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
		    	Create.this.finish();
		    	break;
		    case R.id.addC:
		    	if ((name.length()>0) && (!device.contains("select"))) {
		    		Device newDevice = DataSource.createDevice(name, (device.substring(device.indexOf("(Type:")+7, (device.length()-1))));
			    	adapter.add(newDevice);
				    adapter.notifyDataSetChanged();
				    typeName.setText("");
				    break;
			    }
		    	if (!(name.length()>0)){
					Toast.makeText(getApplicationContext(), "Please enter a name!", Toast.LENGTH_SHORT).show();
					break;
		    	}
		    	else {
					Toast.makeText(getApplicationContext(), "Please select a device type!", Toast.LENGTH_SHORT).show();
					break;
		    	}

		    }
		    }
	  }
	
	  /**
	   * handles the clicks on the list items
	   */
	public void onListItemClick(ListView l, View v, int position, long id) {
		
		//list behavior for room setup
		if(type.equals("room")) {
			ArrayAdapter<Room> adapter = (ArrayAdapter<Room>) getListAdapter();
			Room room = (Room) getListAdapter().getItem(position);
		    Cursor cursor = DataSource.homeMaticDatabase.query(DatabaseHelper.TABLE_CHANNEL,
		            DataSource.allColumnsChannel, DatabaseHelper.COLUMN_CHANNEL_ROOM_ID + " = " + '"' + room.get_id() + '"', null,
		            null, null, null);
			if(cursor.moveToFirst()) {
				Toast.makeText(getApplicationContext(), "Can only delete empty rooms. \b Please migrate existing channels.", Toast.LENGTH_SHORT).show();
			}
			else
			{
				DataSource.deleteRoom(room);
				Toast.makeText(getApplicationContext(), "Deleted " + room.getName() + "!", Toast.LENGTH_SHORT).show();
				adapter.remove(room);
			}

		}
		
		//list behavior for function setup
		if(type.equals("function")) {
			ArrayAdapter<Function> adapter = (ArrayAdapter<Function>) getListAdapter();
			Function function = (Function) getListAdapter().getItem(position);
		    Cursor cursor = DataSource.homeMaticDatabase.query(DatabaseHelper.TABLE_CHANNEL,
		            DataSource.allColumnsChannel, DatabaseHelper.COLUMN_CHANNEL_FUNCTION_ID + " = " + '"' + function.get_id() + '"', null,
		            null, null, null);
			if(cursor.moveToFirst()) {
				Toast.makeText(getApplicationContext(), "Can only delete empty functions. \b Please migrate existing channels.", Toast.LENGTH_SHORT).show();
			}
			else
			{
				DataSource.deleteFunction(function);
				Toast.makeText(getApplicationContext(), "Deleted " + function.getName() + "!", Toast.LENGTH_SHORT).show();
				adapter.remove(function);
			}

		}
		
		//list behavior for device setup
		if(type.equals("device")) {
			ArrayAdapter<Device> adapter = (ArrayAdapter<Device>) getListAdapter();
			Device device = (Device) getListAdapter().getItem(position);
			if (DataSource.deleteDevice(device))
			{
				Toast.makeText(getApplicationContext(), "Deleted " + device.getName() + "!", Toast.LENGTH_SHORT).show();
				adapter.remove(device);
			}
			else {
				Toast.makeText(getApplicationContext(), "Can't delete system channel " + device.getName() + "!", Toast.LENGTH_SHORT).show();
			}
		}
	}
}
