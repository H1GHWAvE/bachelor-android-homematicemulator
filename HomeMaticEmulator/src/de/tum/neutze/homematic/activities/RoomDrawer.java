package de.tum.neutze.homematic.activities;

import java.util.ArrayList;
import java.util.List;

import de.tum.neutze.homematic.database.DataSource;
import de.tum.neutze.homematic.database.Position;
import de.tum.neutze.homematic.server.NanoHTTPD;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Toast;

/**
* @author Johannes Neutze
* @version 1.0
* @since 1.6
*/

public class RoomDrawer extends View implements OnClickListener{

	int radiusFactor = 4;
	int proximityFactor = 2;
	Context overAllContext;
	
	/**
	 * room drawer method
	 * @param context current context
	 */
	public RoomDrawer(Context context) {
		super(context);
		overAllContext = context;

	}	
	
	/**
	 * handles touch events
	 */
	public boolean onTouchEvent (MotionEvent event) {
		List<Position> positions = DataSource.getAllPositions();
		boolean found = false;
        switch (event.getAction()) {
        
            case MotionEvent.ACTION_DOWN :
            	int x_coord = (int) event.getX();
                int y_coord = (int) event.getY();
                for (int i = 0; i < positions.size(); i++){

            		if((x_coord < positions.get(i).getX_coord() + positions.get(i).getBuffer()/proximityFactor && y_coord < positions.get(i).getY_coord() + positions.get(i).getBuffer()/proximityFactor
            				&& x_coord > positions.get(i).getX_coord() - positions.get(i).getBuffer()/proximityFactor && y_coord > positions.get(i).getY_coord() - positions.get(i).getBuffer()/proximityFactor))
            		{
            			//NanoHTTPD.myOut.println("modify");
            			Intent intent_modify = new Intent(getContext(), ChannelEditor.class);
                        intent_modify.putExtra("position_id", String.valueOf(positions.get(i).get_id()));
                        getContext().startActivity(intent_modify);            			
            			found= true;
            			break;
            		}

        		}
                if (found)
                	{
                		invalidate();
                		break;
                	}
               // NanoHTTPD.myOut.println("add");
                Intent intent_add = new Intent(getContext(), ChannelCreator.class);
                intent_add.putExtra("x_coord", String.valueOf((int) event.getX()));
                intent_add.putExtra("y_coord", String.valueOf((int) event.getY()));
                getContext().startActivity(intent_add);
                invalidate();
                break;
                
            	
        }

        return true;

    }
	
	/**
	 * draws the canvas
	 */
    	@SuppressWarnings("deprecation")
		@SuppressLint("NewApi")
		@Override
	protected void onDraw(Canvas c){
	    super.onDraw(c);
	    Bitmap background= BitmapFactory.decodeResource(getResources(), R.drawable.background);
		List<Position> positions = DataSource.getAllPositions();
		Paint paint = new Paint();
		int dwidth = 800;
		int dheight = 480;
		WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		DisplayMetrics metrics = new DisplayMetrics();
		wm.getDefaultDisplay().getMetrics(metrics);
		if(android.os.Build.VERSION.SDK_INT < 17) {
			//dheight = metrics.heightPixels - 100;
			dwidth = display.getWidth();
			dheight = display.getHeight() - 100;
			//dwidth = metrics.widthPixels;
		}
		else {
			//got introduced with API 17, internet said API 13 but didn't work
		    Display newDisplay = getDisplay(); 
			dwidth = newDisplay.getWidth();
		    dheight = newDisplay.getHeight() - 200;
		}
		
	    background = Bitmap.createScaledBitmap(background,dwidth,dheight,true);
		c.drawBitmap(background, 0, 0, null);
		paint.setAntiAlias(true);
		paint.setColor(Color.RED);
		for (int i = 0; i < positions.size(); i++)
			{
				c.drawCircle(positions.get(i).getX_coord(), positions.get(i).getY_coord(), positions.get(i).getBuffer()/radiusFactor, paint);
			}
		
		paint.setColor(Color.RED);		
		paint.setStyle(Paint.Style.FILL);		
		paint.setAntiAlias(true);		
		paint.setTextSize(30);		
		//c.drawText(text, 30, 30, paint);

	}

	/**
	 * not used
	 */
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			
		}
}